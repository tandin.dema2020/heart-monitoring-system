# Heart Monitoring System
### Our STEM project for the year 2024, is to make a heart rate monitoring system:
Plan:
First we’ll work on tinkercad by creating a working model.
Then through tinkercad, we’ll make the project in real life and use its real life applications.

#### Reasons why we chose this project: 
We chose this project because, 
Our heart is a crucial part of our organ system and our body. It is important to note that we have to keep it healthy. Monitoring heart rate regularly can provide insights into our own cardiovascular system. We can also help individuals track their fitness levels and detect potential health issues such as having a normal heart rate or an abnormal heart rate.
By monitoring heart rates especially during physical activities such as morning physical, individuals can set a goal and ensure if they’re working on their goals of optimizing a certain heart rate which can either burn down your fats or reduce the chances of heart disease.

Overtraining, dehydration, or heat exhaustion won’t be a problem when exercising because we can keep track of our heart rates and work accordingly.
This project can help in stress management and reduce the rate of depression amongst students.
As students, most of us don't get enough sleep and even if we do get enough sleep, we usually don't know what we do or what happens when sleeping so this heart rate monitor can also be a sleep monitoring system. 

#### Things we need:
Arduino Board:  Any Arduino board such as Arduino Uno, Arduino Nano, or Arduino Mega will work for this project.

![](https://eeeproject.b-cdn.net/wp-content/uploads/2017/06/Arduino-Uno-board-pins-description.jpg)



Heart Rate Sensor Module: You'll need a heart rate sensor module compatible with Arduino. One commonly used sensor is the Pulse Sensor (PulseSensor.com), which includes an infrared LED and photodetector for measuring heart rate.

![](https://robocraze.com/cdn/shop/products/1_1_8711c087-d290-472f-9b2c-8af431b1b83f.jpg?v=1700214749&width=1445)

Connecting Wires: Jumper wires or breadboard wires to establish connections between the Arduino board and the sensor module.

![](https://blog.sparkfuneducation.com/hubfs/EDU/BLOG%20Images/Jan%202018/JumperWire-Male-01-L.jpg)


Power Source: A power source for the Arduino board, such as a USB cable connected to a computer or a power adapter.

![](https://5.imimg.com/data5/SELLER/Default/2022/7/KX/WA/XO/98847066/uno-cable.jpg)


#### Optional Components:
Breadboard: If you prefer to prototype the circuit on a breadboard.

![](https://cdn-learn.adafruit.com/guides/images/000/001/436/medium800thumb/halfbb_640px.gif)


LEDs and Resistors: Optional for adding visual indicators or feedback based on heart rate readings.

![](https://www.rcpano.net/wp-content/uploads/2023/02/Led-Resistor-Calculater-Single-1.jpg)

Display Module: Optional for displaying heart rate readings, such as an LCD display or OLED display.

![](https://5.imimg.com/data5/SELLER/Default/2023/12/371364416/HT/ZS/WY/562456/liquid-crystal-display-module-500x500.webp)


### Conceptual Sketches:

![Alt text](<Photo on 4-1-2567 BE at 10.59 AM.jpg>)
#### Final project Design:

![Alt text](<Photo on 4-1-2567 BE at 11.23 AM.jpg>)


