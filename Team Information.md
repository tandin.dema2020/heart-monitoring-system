### Introducing our Team members

Meet Team TTRYL

Team members:
1. Lekey Wangmo

![Alt text](<Photo on 4-1-2567 BE at 11.38 AM.jpg>)
Age: 17

Hobbies: Reading, playing volleyball

What I hope to learn : I hope to learn more about coding, get some experience with programing and using different hardware components.

Contribution to the team:I will be cooperative and will help my team mate with drawings, and will also help them with ardiuno programming.

2. Rigsel Wangmo

![Alt text](<Photo on 4-1-2567 BE at 11.49 AM.jpg>)

Age: 15

Hobbies  : Journaling, Reading, Photography, Videography

What I hope to learn: I hope to learn about the way we document things, use more of wokwi and coding in general. I would also like to learn more about the way different sensors work and do different projects related to arduino board.

Contribution to the team: I will help as much as I can and will also learn in the process. I will research on various topics related to the project and find more information on it. I will also try to help with diagrams even if its simple or difficult.

3. Tandin Dema

![Alt text](<Photo on 4-1-2567 BE at 11.55 AM.jpg>)

Age : 17

Hobbies : Reading, playing sports and exploring.

What i hope to learn: I hope to learn programming, and I want to learn how the things work like ardunio and differnt hardware components. 

Contribution to the team: I will help my team as much as I can. I will be doing research and I will learn coding and help my team with coding. 

4. Tshering Choden 

![Alt text](<Photo on 4-1-2567 BE at 12.03 PM.jpg>)

Age : 17
 
 Hobbies: Reading, listening to music, playing sports.
 
 What I hope to learn: I hope to learn more about coding and programming. I also hope to learn more and to do projects related to arduino board. And I also hope to explore differnt hardware componets and how it works.

 Contribution to the team: I will help as much as I can to my team members and at the same time I will learn as much as I can from them. I will help my team with research and gathering informations. I will also help to do hardware components connection.

 5. Yeshey Lhamo
![Alt text](<Photo on 4-1-2567 BE at 12.17 PM.jpg>)

Age : 16

Hobbies : Reading, Drawing, Playing Basketball

What I hope Learn : I aim to concentrate more on coding and delve deeper into learning about Arduino Uno. My goal is to develop exciting projects with Arduino in the future.

Contribution to the Team : My primary focus will be on documentation in GitLab, but I also aim to learn coding and contribute to the project's design.

Our team members : 

![Alt text](<Photo on 4-1-2567 BE at 12.24 PM.jpg>)

Strategies for peer learning :

-Researching

-Discussion on what we have learned

-Making everyone have a part in the project

-Letting everyone have an idea or an experience of what we have done in the process. 

-Feedback and reflections: Teaching eachother when we make mistakes.


[Home page](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Homepage.md)

[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md)

[Project Idea](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Project_Idea.md)