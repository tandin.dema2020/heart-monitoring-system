# Heart Monitoring System
### Introduction:
In the modern day, a common problem that we all face is about health. Health is a crucial part in most of our lives therefore, our main objective for our project is to make our health better in our day to day life. Our project Heart Monitoring System plays a crucial role role in modern healthcare, enabling the monitoring of our pulse. The system utilizes various components such as a pulse sensor and ofcourse an arduino board. Our project can be helpful in many ways because in The Royal Academy, we're constantly moving and our body is constantly in motion. Therfore, it is important to monitor our pulses. Moreover, the fact that we have to do physical activities most of the time, our heart rate can increase or decrease. Even stress can cause high heart rates.

For our inspiration, we were looking for a proper solution for our problem, then we came across a video that inspired us and we were excited to modify and connect our ideas in the project.
The link of the video:
https://youtu.be/1LqBvkHTJXU?si=EUUpJNAZqp-azmPH

In the end, we expect our project to monitor our heart rates. We want it to be useful for everyone in the BHU, in our school campus especially during physical sessions. Our project could also be useful in learning experiences for life science or biology. In conclusion, we want our project to help people with or without heart problems.

[Team Members](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Team%20Information.md)


[Terminology](https://gitlab.com/project-work222511/pulse-sensor/heart-monitoring-system/-/blob/main/Terminology.md)